Categories:Navigation,Sports & Health
License:GPLv3+
Web Site:http://bailu.ch/aat
Source Code:https://github.com/bailuk/AAT
Issue Tracker:https://github.com/bailuk/AAT/issues

Auto Name:AAT
Summary:Track your activities
Description:
GPS-tracking application for tracking sportive activities, with emphasis on
cycling. It uses osmdroid to display map tiles from the OpenStreetMap  project.
.

Repo Type:git
Repo:https://github.com/bailuk/AAT

Build:v0.5-alpha,5
    commit=v0.5-alpha
    target=android-21

Build:v0.6-alpha,6
    commit=v0.6-alpha
    target=android-21

Build:v0.7-alpha,7
    commit=v0.7-alpha
    target=android-21

Build:v0.8-alpha,8
    commit=v0.8-alpha
    target=android-21

Build:v0.9-alpha,9
    commit=v0.9-alpha
    target=android-21

Auto Update Mode:None
Update Check Mode:Tags
Current Version:v0.9-alpha
Current Version Code:9
